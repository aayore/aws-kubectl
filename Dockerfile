FROM alpine

RUN apk update && \
    apk add ca-certificates curl py3-pip && \
    pip install --upgrade pip awscli && \
    KUBECTL_VERSION=$(curl -L -s https://dl.k8s.io/release/stable.txt) && \
    curl --silent -LO "https://dl.k8s.io/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl" -o /usr/local/bin/kubectl && \
    chmod +x /usr/local/bin/kubectl && \
    rm /var/cache/apk/*
